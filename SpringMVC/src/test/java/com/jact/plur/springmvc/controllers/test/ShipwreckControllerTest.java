package com.jact.plur.springmvc.controllers.test;

import com.jact.plur.springmvc.controllers.ShipwreckStub;
import com.jact.plur.springmvc.models.Shipwreck;
import com.jact.plur.springmvc.repositories.ShipwreckRepository;
import com.jact.plur.springmvc.repositories.ShipwreckRepositoryImpl;
import com.jact.plur.springmvc.sqlservice.ShipwreckQueryService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.jact.plur.springmvc.controllers.ShipwreckController;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/*
 * Spring Boot testing with JUnit5
 * https://developer.okta.com/blog/2019/03/28/test-java-spring-boot-junit5
 *
 * The report about tests is to be found in
 * ../SpringMVC/target/site/surefire-report.html
 *
 * mvn surefire-report:report
 *
 * */

@SpringBootTest
@ActiveProfiles("test")
@ExtendWith(SpringExtension.class)
@WebAppConfiguration("classpath:resources")
@EnableWebMvc
@ComponentScan("com.jact.plur.springmvc.config")
@Transactional
class ShipwreckControllerTest {

    private static final Logger log = LoggerFactory.getLogger( ShipwreckControllerTest.class );

    @Autowired
    private WebApplicationContext webApplicationContext;

    @InjectMocks
    private ShipwreckController sc;

    @Autowired
    private ShipwreckRepository shipwreckRepositoryImpl;

    @Autowired
    private ShipwreckQueryService shipwreckQueryServiceImpl;

    protected MockMvc mockMvc;

    @Autowired
    MockHttpServletRequest mockHttpServletRequest;

    @Autowired
    ShipwreckStub shipwreckStub;

    @Value("localhost")
    String serverName;

    @Value("8081")
    Integer serverPort;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        try {

            assertNotNull(this.shipwreckRepositoryImpl);
            assertNotNull( shipwreckStub );

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }

    }

    /***
     * https://attacomsian.com/blog/spring-data-jpa-query-annotation
     *
     * @param depth
     * @return a List of shallow sinkers
     */

    @Test
    @Tag("selects")
    @DisplayName("RETRIEVE shallow sinkers")
    @Rollback(false)
    @Transactional(readOnly = true)
    public void testFindByDepth(@Param("depth")
                @Value("#{new Integer('${app.shallowSinkers}')}") Integer depth ) {

        ArrayList<Shipwreck> allShallowSinkers = new ArrayList<>();
        allShallowSinkers.addAll( shipwreckQueryServiceImpl.findByDepth( depth ));

        log.info( allShallowSinkers.size() + " shallow sinkers were found!" );
        allShallowSinkers.stream().forEach( shipwreck ->
                log.info(shipwreck.getName() + " at " + shipwreck.getDepth() + " m. deep") );
    }

    @Test
    @Tag("updates")
    @DisplayName("Initialize with the STUB test")
    @Rollback(false)
    public void testInitUpdates( TestInfo testInfo ) throws Exception {

        log.info( "SHIPS: " + shipwreckStub.list() );

        if( shipwreckRepositoryImpl.findAndSort(Sort.by(Sort.Direction.DESC, "depth")).isEmpty() ) {
            shipwreckStub.list().forEach(
                    (ship) -> {

                        shipwreckRepositoryImpl.create(ship);
                        log.info( "ADD: " + ship.getName() );
                    });
        } else {
            log.info( "Data found. Exit populating script. " + shipwreckStub.list() );
        }
    }

    @Test
    @Tag("gets")
    @DisplayName("GET all ships REST test")
    public void testAllShips( TestInfo testInfo ) throws Exception {

        log.trace("Testing REST: " + testInfo.getDisplayName() + " started. ");

            assertNotNull(webApplicationContext);
            mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
                    .alwaysDo(MockMvcResultHandlers.print())
                    .build();

            assertNotNull(mockHttpServletRequest);

            mockHttpServletRequest.setCharacterEncoding("UTF-8");
            mockHttpServletRequest.setScheme("http");
            mockHttpServletRequest.setServerName( this.serverName );
            mockHttpServletRequest.setServerPort( this.serverPort );
            mockHttpServletRequest.setRequestURI( "/api/v1/shipwrecks" );

            RequestContextHolder.setRequestAttributes(
                    new ServletRequestAttributes(mockHttpServletRequest));

            log.info( "REQ: " +
                    createURL( mockHttpServletRequest, mockHttpServletRequest.getRequestURI() ) );

            mockMvc.perform(get(mockHttpServletRequest.getRequestURI() )
                    .header("Host", "localhost")
                    .contextPath( mockHttpServletRequest.getContextPath() )
                    .contentType(MediaType.APPLICATION_JSON)
                    .accept(MediaType.APPLICATION_JSON)
                    .with(new RequestPostProcessor() {
                public MockHttpServletRequest postProcessRequest(MockHttpServletRequest request) {
                    request.setServerName( mockHttpServletRequest.getServerName() );
                    return request;
                }}).accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andReturn();
    }

    @Rule
    public ExpectedException expectedEx = ExpectedException.none();

    @Tag("gets")
    @DisplayName("GET from the Repository test")
    public void testSelectShips( TestInfo testInfo ) throws Exception {

        expectedEx.expect(RuntimeException.class);
        log.trace("Testing SELECT: " + testInfo.getDisplayName() + " started. ");
        expectedEx.expectMessage("Testing for NULL as expected");

        try {

            assertNotNull(this.shipwreckRepositoryImpl);
            Optional<Shipwreck> sw = shipwreckRepositoryImpl.findById( 2l );

            log.info( "RSP: " + ( sw.orElseThrow( () -> new RuntimeException( "NULL tested" )) )+ "\n" +
            createURL( mockHttpServletRequest, mockHttpServletRequest.getRequestURI() ));

            log.info( "1st SHIP: " + sw.get().getName() );

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    @BeforeEach
    void init(TestInfo testInfo) {

        String displayName = testInfo.getDisplayName();
        String methodName = testInfo.getTestMethod().get().getName();

    }

    protected static String createURL(HttpServletRequest request, String resourcePath) {

        int port = request.getServerPort();
        StringBuilder result = new StringBuilder();
        result.append(request.getScheme())
                .append("://")
                .append(request.getServerName());

        if ( (request.getScheme().equals("http") && port != 80)
                || (request.getScheme().equals("https") && port != 443) ) {
            result.append(':')
                    .append(port);
        }

        result.append(request.getContextPath());

        if(resourcePath != null && resourcePath.length() > 0) {
            if( ! resourcePath.startsWith("/")) {
                result.append("/");
            }
            result.append(resourcePath);
        }

        return result.toString();

    }

};
