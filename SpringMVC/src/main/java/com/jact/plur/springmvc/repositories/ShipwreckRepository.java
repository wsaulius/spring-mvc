package com.jact.plur.springmvc.repositories;

import com.jact.plur.springmvc.models.Shipwreck;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;

public interface ShipwreckRepository extends CrudRepository<Shipwreck, Long> {

/**
 *  Custom JPQL
 *
 **/
    @Query(value = "SELECT s FROM Shipwreck s ORDER BY :#{#sortBy}" )
    public List<Shipwreck> findAndSort(Sort sortBy);

    /**
     * Custom JPQL with parameters
     **/
    @Query(value = "SELECT s FROM Shipwreck s WHERE s.depth < :depth")
    public Collection<Shipwreck> findByDepth(@Param("depth") int depth);

    public Shipwreck create(Shipwreck ship);
    public List<Shipwreck> findAll();
    public Shipwreck saveAndFlush(Shipwreck shipwreck);
}
