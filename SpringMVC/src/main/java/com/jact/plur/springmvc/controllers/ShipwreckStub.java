package com.jact.plur.springmvc.controllers;

import com.jact.plur.springmvc.config.PersistenceConfiguration;
import com.jact.plur.springmvc.models.Shipwreck;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.util.*;

@Transactional
@RestController
@RequestMapping("api/v1/")
public class ShipwreckStub {

    private static final Logger log = LoggerFactory.getLogger( ShipwreckStub.class );

    private final Map<Long, Shipwreck> wrecks = new HashMap<Long, Shipwreck>();
    private static Long idIndex = 3L;

    @Autowired
    ShipwreckController shipwreckController;

    @Autowired
    public ShipwreckStub()
    { }

    // populate initial ship list
    @PostConstruct
    public void initialize()
    {
        Shipwreck a = new Shipwreck("U869", "A very deep German UBoat", "FAIR", 200, 44.12, 138.44, 1994);
        wrecks.put(1L, a);
        Shipwreck b = new Shipwreck("Thistlegorm", "British merchant boat in the Red Sea", "GOOD", 80, 44.12, 138.44, 1994);
        wrecks.put(2L, b);
        Shipwreck c = new Shipwreck("S.S. Yongala", "A luxury passenger ship wrecked on the great barrier reef", "FAIR", 50, 44.12, 138.44, 1994);
        wrecks.put(3L, c);
    }

    public List<Shipwreck> list() {

        log.debug( "ACCESS: OK" + Arrays.asList( wrecks.values() ) );
        return new ArrayList<Shipwreck>(wrecks.values());
    }

    public Shipwreck create(Shipwreck wreck) {
        idIndex += idIndex;
        wrecks.put(idIndex, wreck);
        return wreck;
    }

    public Shipwreck get(Long id) {
        return wrecks.get(id);
    }

    public Shipwreck update(Long id, Shipwreck wreck) {
        wrecks.put(id, wreck);
        return wreck;
    }

    public Shipwreck delete(Long id) {
        return wrecks.remove(id);
    }

    @RequestMapping(value = "shipwrecks/add", method = RequestMethod.PUT)
    public void add() {
        wrecks.values().forEach( ship -> shipwreckController.create(ship) );
    }

}
