package com.jact.plur.springmvc.sqlservice;

import com.jact.plur.springmvc.models.Shipwreck;
import com.jact.plur.springmvc.repositories.ShipwreckRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class ShipwreckQueryServiceImpl implements ShipwreckQueryService {

    @Autowired
    private ShipwreckRepository repository;

    @Override
    public Collection<Shipwreck> findByDepth(Integer depth) {
        return repository.findByDepth( depth );
    }

}
