package com.jact.plur.springmvc.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Arrays;

@Configuration
@PropertySource("classpath:application-default.properties")
@EnableTransactionManagement
public class PersistenceConfiguration {

    private static final Logger log = LoggerFactory.getLogger( PersistenceConfiguration.class );

    @Autowired
    private Environment env;

    @Bean
    public PlatformTransactionManager transactionManager(){
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        return transactionManager;
    }

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource")
    public DataSource h2DataSource() {

        log.info ( "PROFILES: " + Arrays.toString( env.getActiveProfiles() ) );
        log.info ( "DEFAULTS: " + Arrays.toString( env.getDefaultProfiles()) );
        log.debug( "SET Driver: " + env.getProperty("driver-class-name") );

        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        return dataSource;
    }
}
