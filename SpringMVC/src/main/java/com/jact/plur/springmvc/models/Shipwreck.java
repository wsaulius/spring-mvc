package com.jact.plur.springmvc.models;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonRawValue;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;

@Table(name = "SHIPWRECK", uniqueConstraints = {@UniqueConstraint(columnNames = "NAME")})
@Entity
public class Shipwreck {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    String name;
    String description;

    @Column(name = "conditionn")
    String condition;

    @JsonRawValue
    Integer depth;

    @JsonRawValue
    Double latitude;

    @JsonRawValue
    Double longitude;

    @JsonRawValue
    Integer yearDiscovered;

    public Shipwreck() {
    }

    public Shipwreck(String name, String description, String condition, Integer depth, Double latitude, Double longitude, Integer yearDiscovered) {

        this.name = name;
        this.description = description;
        this.condition = condition;
        this.depth = depth;
        this.latitude = latitude;
        this.longitude = longitude;
        this.yearDiscovered = yearDiscovered;
    }

    @JsonGetter
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @JsonGetter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @JsonGetter
    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    @JsonGetter
    public Integer getDepth() {
        return depth;
    }

    public void setDepth(Integer depth) {
        this.depth = depth;
    }

    @JsonGetter
    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @JsonGetter
    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    @JsonGetter
    public Integer getYearDiscovered() {
        return yearDiscovered;
    }

    public void setYearDiscovered(Integer yearDiscovered) {
        this.yearDiscovered = yearDiscovered;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString( this );
        } catch (JsonProcessingException e) {
            return new String("{}");
        }
    }


}
