package com.jact.plur.springmvc.sqlservice;

import com.jact.plur.springmvc.models.Shipwreck;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public
interface ShipwreckQueryService {

    public Collection<Shipwreck> findByDepth(Integer depth);

}
