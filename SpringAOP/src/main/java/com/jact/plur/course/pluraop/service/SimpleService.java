package com.jact.plur.course.pluraop.service;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
@Service
public class SimpleService {

    public void doSomething() {

    }
}
