package com.jact.plur.course.pluraop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PlurAopApplication {

    public static void main(String[] args) {
        SpringApplication.run(PlurAopApplication.class, args);
    }
}
