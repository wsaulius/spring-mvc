package sda.springboot.exercises.springbootdemo;

import net.bytebuddy.implementation.bytecode.Throw;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sda.springboot.exercises.springbootdemo.config.AppConfig;
import sda.springboot.exercises.springbootdemo.services.MyValidationService;
import sda.springboot.exercises.springbootdemo.services.MyValidationServiceImpl;

@SpringBootApplication
public class SpringbootDemoApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger( SpringbootDemoApplication.class );

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDemoApplication.class, args);
	}

	public void run(String... args) throws Exception {
		AnnotationConfigApplicationContext context =
				new AnnotationConfigApplicationContext();
		context.register(AppConfig.class);
		context.refresh();

		MyValidationService validationService =
				context.getBean(MyValidationServiceImpl.class);

		/* BAD programming practice
		try {
		validationService.validate( args[0] );
		}
		catch (Throwable t ) {
			log.error( "Error when executing. " );
			t.printStackTrace();
		}
	    */

		/* OK programming practice
		try {
		validationService.validate( args[0] );
		}
		catch (ArrayIndexOutOfBoundsException e ) {
			log.error( "Error when indexing (most likely you're not passing cmd parameter)." );
			log.debug( e.getMessage() );
			e.printStackTrace();
 		}*/

		/* GOOD programming practice */
		try {
			// validationService.validate( args[0] );
		}
		catch (ArrayIndexOutOfBoundsException e ) {
			log.error( e.getMessage() );
			throw new Exception(
					"Error when indexing (most likely you're not passing cmd parameter)", e );
		}


	}
}