package sda.springboot.exercises.springbootdemo.config;

import org.springframework.context.annotation.Bean;
import sda.springboot.exercises.springbootdemo.services.MyValidationService;
import sda.springboot.exercises.springbootdemo.services.MyValidationServiceImpl;

public class AppConfig {

    @Bean
    public MyValidationService getService(){
        return new MyValidationServiceImpl();
    }


}
