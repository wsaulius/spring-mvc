package sda.springboot.exercises.springbootdemo.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import sda.springboot.exercises.springbootdemo.services.MyValidationService;

@Service
public class MyValidationServiceImpl implements MyValidationService {

    private static final Logger log = LoggerFactory.getLogger( MyValidationServiceImpl.class );

    private String validName = "Valdas";

    @Override
    public boolean validate(String cmdname) {

        log.info( "About to enter secret validation service " + this.getClass() );
        log.debug( "Name to validate: " + cmdname );
        return validName.equalsIgnoreCase( cmdname );
    }

}
