package sda.springboot.exercises.springbootdemo.services;

public interface MyValidationService {

     boolean validate( final String cmdname );
}
