package com.jact.plur.springjpa;

import com.jact.plur.springjpa.model.Manufacturer;
import com.jact.plur.springjpa.repository.ManufacturerJpaRepository;
import com.jact.plur.springjpa.repository.ManufacturerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ManufacturerPersistenceTests {

    private static final Logger log = LoggerFactory.getLogger( ManufacturerPersistenceTests.class );

    @Autowired
    private ManufacturerRepository manufacturerRepository;

    @Autowired
    private ManufacturerJpaRepository manufacturerJpaRepository;

    @Test
    public void testTrueFalse() {
        List<Manufacturer> mans = manufacturerJpaRepository.findByActiveTrue();
        assertEquals("Fender Musical Instruments Corporation", mans.get(0).getName());

        mans = manufacturerJpaRepository.findByActiveFalse();
        assertEquals("Gibson Guitar Corporation", mans.get(0).getName());
    }

    @Test
    public void testGetManufacturersFoundedBeforeDate() {
        List<Manufacturer> mans = manufacturerRepository.getManufacturersFoundedBeforeDate(new Date());
        assertEquals(2, mans.size());
    }

    @Test
    public void testGetManufactureByName() {

        final String byMaker = "Fender";

        final List byMakerResults =
            manufacturerRepository.getManufacturersByName( byMaker );
        log.info( "getManufacturersByName " + byMakerResults.size() );

    }

    @Test
    public void testGetManufacturersThatSellModelsOfType() {
        List<Manufacturer> mans = manufacturerRepository.getManufacturersThatSellModelsOfType("Semi-Hollow Body Electric");
        assertEquals(1, mans.size());
    }
}
