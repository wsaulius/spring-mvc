package com.jact.plur.springjpa;

import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringjpaApplication.class)
@AutoConfigureMockMvc
public class ByPriceAPITest {

    private static final Logger log = LoggerFactory.getLogger( ByPriceAPITest.class );

    @Autowired
    private MockMvc mockMvc;

    @Value( value = "/api/${application.api.vr}/" )  // Same as v1
    String versioningAPI;

    /*
     * Spring Boot testing with JUnit5
     * https://developer.okta.com/blog/2019/03/28/test-java-spring-boot-junit5
     *
     * The report about tests is to be found in
     * ../SpringMVC/target/site/surefire-report.html
     *
     * mvn surefire-report:report
     *
     * */

    @Test
    @Tag("gets")
    @DisplayName("GET the models by price MVC API test")
    public void testSelectByPrice() throws Exception {

        try {

            /*
            mockMvc.perform(get("http://localhost:8080/api/v1/models?price=600")
                    .contentType("application/json"))
                    .andExpect(status().isOk());

             */

            mockMvc.perform(get( versioningAPI + "/models")
                    .contentType("application/json").param( "price", "600"))
                    .andExpect(status().isOk());

        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

}
