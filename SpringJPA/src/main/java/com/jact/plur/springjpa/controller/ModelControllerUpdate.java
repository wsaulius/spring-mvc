package com.jact.plur.springjpa.controller;

import com.jact.plur.springjpa.model.Model;
import com.jact.plur.springjpa.repository.ModelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 *
 * https://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html
 *
 * */

@Profile("default")
@RequestMapping("api/${application.api.vr}/")
@Controller
public class ModelControllerUpdate {

    @Autowired
    @Qualifier("byModelRepository")
    private ModelRepository modelRepository;

    private static final Logger log = LoggerFactory.getLogger( ModelControllerUpdate.class );

    @GetMapping("/by-price")
    public String byPriceForm(org.springframework.ui.Model model,
                              @PathVariable("application.api.vr") String parameter) {

        // Set a guitar
        model.addAttribute("guitar", new com.jact.plur.springjpa.model.Model());
        return "models-by-prices";
    }

    @PostMapping("/by-price")
    public String postByPriceRange(@ModelAttribute com.jact.plur.springjpa.model.Model guitar,
                                   @Value("/api/{application.api.vr}/models") String api_models ) {

        log.info("Guitar search in range {} has been initiated.", guitar.getPrice());

        Optional<com.jact.plur.springjpa.model.Model> optionalModel;

        try {
            optionalModel =
                    Optional.of( modelRepository.getModelsInPriceRange(
                            BigDecimal.ONE, guitar.getPrice()).get(0) );

            log.info( optionalModel.get().toString() );

            return String.format( "redirect:%s?price=%s", api_models,
                    optionalModel.get().getPrice().intValue());

        } catch ( IndexOutOfBoundsException e ) {
            return "welcome";
        }

    }
}