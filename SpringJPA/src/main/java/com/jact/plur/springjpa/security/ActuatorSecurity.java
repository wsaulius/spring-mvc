package com.jact.plur.springjpa.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.autoconfigure.security.servlet.EndpointRequest;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.access.AccessDeniedHandler;

@Configuration
@EnableWebSecurity
public class ActuatorSecurity extends WebSecurityConfigurerAdapter {

    @Autowired
    private AccessDeniedHandler accessDeniedHandler;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests().requestMatchers(
                EndpointRequest.toAnyEndpoint()).authenticated().anyRequest().permitAll()
                .and().formLogin().loginPage("/login")
                .and()
                .exceptionHandling().accessDeniedPage("/403");

                    http.csrf().disable()
                    .authorizeRequests()
                    .antMatchers("/api/v1").denyAll()
                    .antMatchers("/api/v2").permitAll()
                    .and()

                            .formLogin()
                            .loginPage("/login")
                            .permitAll()
                            .and()

                    .logout().permitAll()
                    .and()
                    .exceptionHandling().accessDeniedPage("/403");

        /*
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers("/", "/home", "/about").permitAll()
                .antMatchers("/admin/**").hasAnyRole("ADMIN")
                .antMatchers("/user/**").hasAnyRole("USER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling().accessDeniedHandler(accessDeniedHandler);

         */
    }
}


