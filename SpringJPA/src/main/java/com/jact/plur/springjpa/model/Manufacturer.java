package com.jact.plur.springjpa.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@NamedNativeQuery(name = "Manufacturer.getAllThatSellAcoustics",
        query = "SELECT m.id, m.name, m.founded_date, m.average_yearly_sales, m.location_id as headquarters_id, m.active "
                + "FROM manufacturer m "
                + "LEFT JOIN model model ON (m.id = model.manufacturer_id) "
                + "LEFT JOIN modeltype mt ON (mt.id = model.modeltype_id) "
                + "WHERE (mt.name = ?)", resultClass = Manufacturer.class)
public class Manufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Date foundedDate;
    private BigDecimal averageYearlySales;
    private Boolean active;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "MANUFACTURER_ID")
    private List<Model> models = new ArrayList<Model>();

    @ManyToOne
    private Location headquarters;

    @JsonGetter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonGetter
    public Date getFoundedDate() {
        return foundedDate;
    }

    public void setFoundedDate(Date foundedDate) {
        this.foundedDate = foundedDate;
    }

    @JsonGetter
    public BigDecimal getAverageYearlySales() {
        return averageYearlySales;
    }

    public void setAverageYearlySales(BigDecimal averageYearlySales) {
        this.averageYearlySales = averageYearlySales;
    }

    @JsonGetter
    public List<Model> getModels() {
        return models;
    }

    public void setModels(List<Model> models) {
        this.models = models;
    }

    @JsonGetter
    public Location getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(Location headquarters) {
        this.headquarters = headquarters;
    }

    @JsonGetter
    public Long getId() {
        return id;
    }

    @JsonGetter
    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public String toString() {
        try {
            return new ObjectMapper().writeValueAsString(this);
        } catch (JsonProcessingException e) {
            return new String("{}");
        }
    }
}
