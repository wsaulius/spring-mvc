package com.jact.plur.springjpa.repository;

import com.jact.plur.springjpa.model.ModelType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ModelTypeJpaRepository extends JpaRepository<ModelType, Long> {
    List<ModelType> findByNameIsNull();
}
