package com.jact.plur.springjpa.controller;

import com.jact.plur.springjpa.model.Model;
import com.jact.plur.springjpa.repository.ModelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

@Profile("test")
@RestController
@RequestMapping("api/${application.api.vr}/")
public class ModelControllerOlder {

    @Autowired
    @Qualifier("byModelRepository")
    private ModelRepository modelRepository;

    @RequestMapping(value = "models", method = RequestMethod.GET)
    public List<Model> listByPriceRange( @RequestParam String fromPrice,
                                         @RequestParam String toPrice) {

        return
        modelRepository.getModelsInPriceRange(
                new BigDecimal(Integer.parseInt( fromPrice )),
                new BigDecimal( Integer.parseInt( toPrice )));
    }

}
