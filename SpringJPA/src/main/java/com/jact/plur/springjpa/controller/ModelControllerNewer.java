package com.jact.plur.springjpa.controller;

import com.jact.plur.springjpa.model.Model;
import com.jact.plur.springjpa.repository.ModelRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

/**
 *
 * https://www.thymeleaf.org/doc/tutorials/2.1/thymeleafspring.html
 *
 * */

@Profile("default")
@RestController
@RequestMapping("api/${application.api.vr}/")   // Same as v2
public class ModelControllerNewer {

    @Autowired
    @Qualifier("byModelRepository")
    private ModelRepository modelRepository;

    private static final Logger log = LoggerFactory.getLogger( ModelControllerUpdate.class );

    @RequestMapping(value = "models", method = RequestMethod.GET)
    public List<Model> listByPriceRange( @RequestParam String price) {

        return
                modelRepository.getModelsInPriceRange(
                        BigDecimal.ONE,
                        new BigDecimal( Integer.parseInt( price )));
    }
}