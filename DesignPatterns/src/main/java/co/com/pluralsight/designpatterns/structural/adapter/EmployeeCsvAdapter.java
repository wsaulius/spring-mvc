package co.com.pluralsight.designpatterns.structural.adapter;

public class EmployeeCsvAdapter implements Employee {

    private EmployeeCSV instance;

    public EmployeeCsvAdapter(EmployeeCSV instance) {
        this.instance =instance;
    }

    @Override
    public String getId() {
        return String.valueOf(instance.getId());
    }

    @Override
    public String getFirstName() {
        return instance.getFirstName();
    }

    @Override
    public String getLastName() {
        return instance.getLastName();
    }

    @Override
    public String getEmail() {
        return instance.getEmailAddress();
    }


    @Override
    public String toString() {
        return "EmployeeCsvAdapter{" +
                "ID=" + instance.getId() +
                '}';
    }
}
