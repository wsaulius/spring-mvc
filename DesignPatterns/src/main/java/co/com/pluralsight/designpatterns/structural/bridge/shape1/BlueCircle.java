package co.com.pluralsight.designpatterns.structural.bridge.shape1;

public class BlueCircle extends Circle {
    @Override
    public void applyColor() {
        System.out.println("Applying blue color");
    }
}
