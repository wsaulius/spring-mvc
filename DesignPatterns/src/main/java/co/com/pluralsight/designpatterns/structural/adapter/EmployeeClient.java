package co.com.pluralsight.designpatterns.structural.adapter;

import java.util.ArrayList;
import java.util.List;

public class EmployeeClient {

    public List<Employee> getEmployeeList() {
        List<Employee> employees = new ArrayList<>();
       Employee employeeFomDB = new EmployeeDB("123","john","wick","john@wick.com");

       employees.add(employeeFomDB);

       EmployeeLdap employeeFromLdap = new EmployeeLdap("chewie","Solo","Han","han@solo.com");

       employees.add(new EmployeeLdapAdapter(employeeFromLdap));

       EmployeeCSV employeeFromCSV = new EmployeeCSV("568,Sherlock,Holmes,sherlock@holmes.com");

       employees.add(new EmployeeCsvAdapter(employeeFromCSV));

       return employees;
    }
}
