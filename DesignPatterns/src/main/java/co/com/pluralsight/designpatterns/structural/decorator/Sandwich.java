package co.com.pluralsight.designpatterns.structural.decorator;

public interface Sandwich {

    String make();

}
