package co.com.pluralsight.designpatterns.structural.decorator;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

public class DecoratorEverydayDemo {

    public static void main(String ... args) throws Exception{
        File file = new File("D:\\Projects\\Courses\\PluralsightCourses\\DesignPatterns\\src\\main\\java\\co\\com\\pluralsight\\designpatterns\\structural\\decorator\\output.txt");
        file.createNewFile();

        OutputStream oStream = new FileOutputStream(file);
        DataOutputStream doStream = new DataOutputStream(oStream);
        doStream.writeChars("text");

        doStream.close();
        oStream.close();

    }

}
