package co.com.pluralsight.designpatterns.creational.abstractfactory;

public interface Validator {
    boolean isValid(CreditCard creditCard);
}
