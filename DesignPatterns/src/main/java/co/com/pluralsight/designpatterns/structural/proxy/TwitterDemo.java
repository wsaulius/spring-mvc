package co.com.pluralsight.designpatterns.structural.proxy;

public class TwitterDemo {
    public static void main(String ... args){
        TwitterService service = (TwitterService) SecurityProxy.newIntance(new TwitterServiceStub());

        System.out.println(service.getTimeline("wherever"));
    }
}
